<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use App\Models\Member;


class MemberSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $members = [
            [
                'name' => 'Edwin Mahendra',
                'univ' => 'Universitas Kristen Duta Wacana',
                'asal' => 'Yogyakarta'
            ],
            [
                'name' => 'Mikael Rizki',
                'univ' => 'Universitas Kristen Duta Wacana',
                'asal' => 'Yogyakarta'
            ],
            [
                'name' => 'Imanuel Vicky Sanjaya',
                'univ' => 'Universitas Kristen Duta Wacana',
                'asal' => 'Tegal'
            ],
            [
                'name' => 'Novianto',
                'univ' => 'Universitas Atma Jaya',
                'asal' => 'Jombang'
            ],
            [
                'name' => 'Ryzal',
                'univ' => 'UPNV',
                'asal' => 'Sumedang'
            ],
            [
                'name' => 'Fathur',
                'univ' => 'UPNV',
                'asal' => 'Sumedang'
            ],
            [
                'name' => 'Evan',
                'univ' => 'Universitas Atma Jaya',
                'asal' => 'Sumedang'
            ],
            [
                'name' => 'Christo',
                'univ' => 'Universitas Atma Jaya',
                'asal' => 'Tegal'
            ],
            [
                'name' => 'Bagas',
                'univ' => 'Universitas Atma Jaya',
                'asal' => 'Sumedang'
            ],
            [
                'name' => 'Samuel',
                'univ' => 'Universitas Kristen Duta Wacana',
                'asal' => 'Sumedang'
            ],
        ];

        DB::beginTransaction();

        foreach ($members as $member) {
            Member::firstorCreate($member);
        }

        DB::commit();
    }
}