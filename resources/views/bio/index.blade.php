@extends('layout/main')

@section('menu-bio', 'active')
@section('menu-title', 'Bio')
@section('menu-route-title', 'Bio')
@section('menu-title-bar', 'AdminLTE3 - Bio')

@section('content')

    <div class="container-fluid py-1">
        <div class="row g-4">
            <div class="col-md-4">
                <div class="card shadow-lg">
                    <img src="https://media.licdn.com/dms/image/D5603AQGXyjlUQVfWCA/profile-displayphoto-shrink_800_800/0/1708012861716?e=1717027200&v=beta&t=XQ7H9ZcEcnuCG8T7g_yx8a1dFD3ug_kMhVKAYsmgop8"
                        class="card-img-top rounded-top" alt="Profile Picture"
                        style="border-bottom: 1px solid rgba(0,0,0,.125);">
                    <div class="card-body text-center">
                        <h4 class="card-text text-center"><strong>Edwin Mahendra</strong></h4>
                        <p class="card-text text-center">Software Developer with a good manner</p>
                    </div>
                </div>
            </div>

            <div class="col-md-8">
                <div class="card mb-3 shadow">
                    <div class="card-header bg-primary text-white">
                        <i class="fas fa-user"></i> Informasi Pribadi
                    </div>
                    <div class="card-body">
                        <p><strong>Nama Lengkap :</strong> Edwin Mahendra</p>
                        <p><strong>Tanggal Lahir :</strong> 18 Maret 2002</p>
                        <p><strong>Email :</strong> edwin@example.com</p>
                        <p><strong>LinkedIn :</strong> linkedin.com/in/edwinmahendra</p>
                    </div>
                </div>
                <div class="card mb-3 shadow">
                    <div class="card-header bg-primary text-white">
                        <i class="fas fa-briefcase"></i> Ringkasan Profil
                    </div>
                    <div class="card-body">
                        <p>Seorang Software Developer yang bersemangat dengan lebih dari 4 tahun pengalaman dalam
                            pengembangan web dan mobile apps. Spesialisasi dalam JavaScript dan framework seperti React dan
                            Angular. Memiliki track record yang terbukti dalam meningkatkan performa aplikasi dan
                            menyediakan solusi efisien untuk masalah kompleks.</p>
                    </div>
                </div>
                <div class="card mb-3 shadow">
                    <div class="card-header bg-primary text-white">
                        <i class="fas fa-graduation-cap"></i> Riwayat Pendidikan
                    </div>
                    <div class="card-body">
                        <ul class="list-unstyled">
                            <li><strong>2018-2022: </strong> Sarjana Komputer, Universitas Kristen Duta Wacana</li>
                            <li><strong>2015-2018: </strong> SMA Negeri 100 Yogyakarta</li>
                        </ul>
                    </div>
                </div>
                <div class="card mb-3 shadow">
                    <div class="card-header bg-primary text-white">
                        <i class="fas fa-history"></i> Pengalaman Kerja
                    </div>
                    <div class="card-body">
                        <ul class="list-unstyled">
                            <li><strong>2022-Sekarang: </strong> Software Developer di XYZ Corp</li>
                            <li><strong>2020-2022: </strong> Intern Software Developer di ABC Startup</li>
                        </ul>
                    </div>
                </div>
                <div class="card mb-3 shadow">
                    <div class="card-header bg-primary text-white">
                        <i class="fas fa-tools"></i> Keahlian dan Kualifikasi
                    </div>
                    <div class="card-body">
                        <p>Proficient in: JavaScript, React, Angular, Node.js, SQL, NoSQL, Docker, Kubernetes.</p>
                    </div>
                </div>
                <div class="card mb-3 shadow">
                    <div class="card-header bg-primary text-white">
                        <i class="fas fa-certificate"></i> Sertifikasi dan Pelatihan
                    </div>
                    <div class="card-body">
                        <ul class="list-unstyled">
                            <li><strong>2021: </strong> Certified Kubernetes Administrator</li>
                            <li><strong>2020: </strong> React Development Course, Codecademy</li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection
