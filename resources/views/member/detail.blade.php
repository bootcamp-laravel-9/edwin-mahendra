@extends('layout/main')

@section('menu-title', $isEditMode ? 'Edit' : 'Detail')
@section('menu-route-title', $isEditMode ? 'Edit' : 'Detail')
@section('menu-title-bar', $isEditMode ? 'AdminLTE3 - Edit' : 'AdminLTE3 - Detail')

@section('content')
    <div class="card card-primary">
        <div class="card-header">
            <h3 class="card-title">{{ $isEditMode ? 'Edit' : 'Detail' }} of {{ $detail->name }}</h3>
        </div>

        <form action="{{ url('/edit-member-process') }}" method="POST">
            @csrf

            <div class="card-body">
                <input type="hidden" name="id" value="{{ $detail->id }}">
                <div class="form-group">
                    <label for="name">Name</label>
                    <input @disabled(!$isEditMode) type="text" class="form-control" id="name" name="name"
                        value="{{ $detail->name }}">
                </div>
                <div class="form-group">
                    <label for="univ">University</label>
                    <input @disabled(!$isEditMode) type="text" class="form-control" id="univ" name="univ"
                        value="{{ $detail->univ }}">
                </div>
                <div class="form-group">
                    <label for="asal">Region</label>
                    <input @disabled(!$isEditMode) type="text" class="form-control" id="asal" name="asal"
                        value="{{ $detail->asal }}">
                </div>
            </div>

            @if ($isEditMode)
                <div class="card-footer">
                    <button type="submit" class="btn btn-primary">Update</button>
                </div>
            @endif
        </form>

    </div>
@endsection
