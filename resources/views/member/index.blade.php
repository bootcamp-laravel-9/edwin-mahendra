@extends('layout/main')
@section('menu-data', 'active')
@section('menu-title', 'Tabel')
@section('menu-route-title', 'Tabel')
@section('title', 'Data Bootcamp - Tabel')
@section('menu-title-bar', 'AdminLTE3 - Data Bootcamp')
@section('content')

    <div class="card">
        <div class="card-header">
            <h3 class="card-title">Data Bootcamp - Front End</h3>
        </div>
        <!-- /.card-header -->
        <div class="card-body">
            <table class="table table-bordered">
                <thead>
                    <tr>
                        <th style="width: 5%">No</th>
                        <th style="width: 25%">Nama</th>
                        <th style="width: 30%">Asal Kampus</th>
                        <th style="width: 20%">Asal Daerah</th>
                        <th style="width: 3%">Aksi</th> <!-- Adjusted the width here -->
                    </tr>
                </thead>
                <tbody>
                    @foreach ($data as $key => $value)
                        <tr>
                            <td>{{ $loop->iteration }}</td>
                            <td>{{ $value['name'] }}</td>
                            <td>
                                <span
                                    style="color: {{ $colorMap[$value['univ']] ?? $colorMap['default'] }}">{{ $value['univ'] }}</span>
                            </td>
                            <td>{{ $value['asal'] }}</td>
                            <td>
                                <div class="btn-group" role="group" aria-label="Action Buttons" style="gap: 5px;">
                                    <a href="{{ url('/member-detail/' . $value->id) }}" class="btn btn-primary btn-sm"
                                        style="border-radius: .25rem;">
                                        <i class="fas fa-eye"></i>
                                    </a>
                                    <a href="{{ url('/member-edit/' . $value->id) }}" class="btn btn-secondary btn-sm"
                                        style="border-radius: .25rem;">
                                        <i class="fas fa-edit"></i>
                                    </a>
                                    <a href="{{ url('/member-delete/' . $value->id) }}" class="btn btn-danger btn-sm"
                                        style="border-radius: .25rem;"
                                        onclick="return confirm('Are you sure you want to delete this item?');">
                                        <i class="fas fa-trash"></i>
                                    </a>
                                </div>
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
        {{-- <!-- /.card-body -->
    <div class="card-footer clearfix">
        <ul class="pagination pagination-sm m-0 float-right">
            <li class="page-item"><a class="page-link" href="#">&laquo;</a></li>
            <li class="page-item"><a class="page-link" href="#">1</a></li>
            <li class="page-item"><a class="page-link" href="#">2</a></li>
            <li class="page-item"><a class="page-link" href="#">3</a></li>
            <li class="page-item"><a class="page-link" href="#">&raquo;</a></li>
        </ul>
    </div> --}}
    </div>
@endsection
