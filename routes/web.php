<?php

use Illuminate\Support\Facades\Route;
// use App\Http\Controllers\UserController;
// use App\Http\Controllers\BioController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });

Route::get('/', function () {
    return view('dashboard.index');
});
// Route::get('/databootcamp', [UserController::class, 'index']);
Route::get('/member', 'UserController@index');
Route::get('/bio', 'BioController@index');
Route::get('/member-detail/{id}', 'UserController@show');
Route::get('/member-edit/{id}', 'UserController@edit');
Route::post('/edit-member-process', 'UserController@update');
Route::get('/member-delete/{id}', 'UserController@delete');
// Existing login route
Route::get('/login', 'AuthController@login')->name('login');
Route::get('/register', 'AuthController@register')->name('register');
// Route::get('/register', function () {
//     return view('auth.register');
// })->name('register');