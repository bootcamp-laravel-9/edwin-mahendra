<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Member;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // $data = [
        //     ['nama' => 'Edwin Mahendra', 'univ' => 'Universitas Kristen Duta Wacana', 'asal' => 'Yogyakarta' ],
        //     ['nama' => 'Mikael Rizki', 'univ' => 'Universitas Kristen Duta Wacana', 'asal' => 'Yogyakarta' ],
        //     ['nama' => 'Imanuel Vicky Sanjaya', 'univ' => 'Universitas Kristen Duta Wacana', 'asal' => 'Tegal' ],
        //     ['nama' => 'Novianto', 'univ' => 'Universitas Atma Jaya', 'asal' => 'Jombang' ],
        //     ['nama' => 'Ryzal', 'univ' => 'UPNV', 'asal' => 'Sumedang' ],
        //     ['nama' => 'Fathur', 'univ' => 'UPNV', 'asal' => 'Sumedang' ],
        //     ['nama' => 'Evan', 'univ' => 'Universitas Atma Jaya', 'asal' => 'Sumedang' ],
        //     ['nama' => 'Christo', 'univ' => 'Universitas Atma Jaya', 'asal' => 'Tegal' ],
        //     ['nama' => 'Bagas', 'univ' => 'Universitas Atma Jaya', 'asal' => 'Sumedang' ],
        //     ['nama' => 'Samuel', 'univ' => 'Universitas Kristen Duta Wacana', 'asal' => 'Sumedang' ],
        // ];

        $colorMap = [
        'Universitas Kristen Duta Wacana' => 'blue',
        'UPNV' => 'red',
        'Universitas Atma Jaya' => 'green', 
    ];

        $data = Member::all();
        return View('member.index', compact('data', 'colorMap'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $detail = Member::findOrFail($id);
        return view('member.detail', ['detail' => $detail, 'isEditMode' => false]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $detail = Member::findOrFail($id);
        return view('member.detail', ['detail' => $detail, 'isEditMode' => true]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Member $member)
    {
        $updateMember = $member -> find($request->id);
        $updateMember->id = $request->id;
        $updateMember->name = $request->name;
        $updateMember->univ = $request->univ;
        $updateMember->asal = $request->asal;
        $updateMember->save();
        return redirect('/member');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function delete($id)
    {
        //
        $member = Member::destroy($id);
        return redirect('/member');
    }
}